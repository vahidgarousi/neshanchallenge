package ir.vbile.app.neshanchallenge.core.domain.models.routing;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RoutesItem{

	@SerializedName("legs")
	private List<LegsItem> legs;

	@SerializedName("overview_polyline")
	private OverviewPolyline overviewPolyline;

	public List<LegsItem> getLegs(){
		return legs;
	}

	public OverviewPolyline getOverviewPolyline(){
		return overviewPolyline;
	}
}