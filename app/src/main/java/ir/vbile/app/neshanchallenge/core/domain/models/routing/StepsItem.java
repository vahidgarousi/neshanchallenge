package ir.vbile.app.neshanchallenge.core.domain.models.routing;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class StepsItem{

	@SerializedName("duration")
	private Duration duration;

	@SerializedName("start_location")
	private List<Double> startLocation;

	@SerializedName("distance")
	private Distance distance;

	@SerializedName("instruction")
	private String instruction;

	@SerializedName("name")
	private String name;

	@SerializedName("polyline")
	private String polyline;

	@SerializedName("maneuver")
	private String maneuver;

	public Duration getDuration(){
		return duration;
	}

	public List<Double> getStartLocation(){
		return startLocation;
	}

	public Distance getDistance(){
		return distance;
	}

	public String getInstruction(){
		return instruction;
	}

	public String getName(){
		return name;
	}

	public String getPolyline(){
		return polyline;
	}

	public String getManeuver(){
		return maneuver;
	}
}