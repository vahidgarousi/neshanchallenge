package ir.vbile.app.neshanchallenge.feature_navigator.data.data_store.remote;


import io.reactivex.Single;
import ir.vbile.app.neshanchallenge.BuildConfig;
import ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding.NeshanAddress;
import ir.vbile.app.neshanchallenge.core.domain.models.routing.DirectionWithCar;
import ir.vbile.app.neshanchallenge.core.util.Constants;
import ir.vbile.app.neshanchallenge.feature_navigator.data.DirectionMode;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NavigatorApi {
    @GET("direction")
    Single<DirectionWithCar> getDirections(
            @Query("type") @DirectionMode String type,
            @Query("origin") String origin,
            @Query("destination") String destination
    );

    @GET("reverse")
    Single<NeshanAddress> getReverseGeocoding(
            @Query("lat") double latitude,
            @Query("lng") double longitude
    );

    String BASE_URL = BuildConfig.STAGE_BASE_URL;
}
