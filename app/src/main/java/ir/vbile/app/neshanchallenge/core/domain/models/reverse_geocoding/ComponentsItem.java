package ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding;

import com.google.gson.annotations.SerializedName;

public class ComponentsItem{

	@SerializedName("name")
	private String name;

	@SerializedName("type")
	private String type;

	public String getName(){
		return name;
	}

	public String getType(){
		return type;
	}
}