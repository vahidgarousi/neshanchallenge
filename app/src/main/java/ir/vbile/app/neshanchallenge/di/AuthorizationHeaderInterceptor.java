package ir.vbile.app.neshanchallenge.di;

import androidx.annotation.NonNull;

import java.io.IOException;

import javax.inject.Inject;

import ir.vbile.app.neshanchallenge.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationHeaderInterceptor implements Interceptor {
    @Inject
    AuthorizationHeaderInterceptor() {
    }

    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest = request.newBuilder()
                .addHeader("Api-Key", BuildConfig.NESHAN_API_KEY)
                .build();
        return chain.proceed(newRequest);
    }
}
