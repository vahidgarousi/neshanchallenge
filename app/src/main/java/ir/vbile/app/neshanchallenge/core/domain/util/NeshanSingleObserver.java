package ir.vbile.app.neshanchallenge.core.domain.util;

import androidx.annotation.NonNull;

import io.reactivex.SingleObserver;

public abstract class NeshanSingleObserver<T> implements SingleObserver<T> {
    @Override
    public void onError(@NonNull Throwable e) {
    }
}
