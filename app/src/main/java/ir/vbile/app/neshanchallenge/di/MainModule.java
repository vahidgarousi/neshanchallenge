package ir.vbile.app.neshanchallenge.di;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import ir.vbile.app.neshanchallenge.feature_navigator.presentation.navigate_dialog.RouteSuggestionDialog;

@Module
@InstallIn(SingletonComponent.class)
public class MainModule {
    @Provides
    @Singleton
    public static RouteSuggestionDialog provideRouteSuggestionDialog() {
        return new RouteSuggestionDialog();
    }
}
