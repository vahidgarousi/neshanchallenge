package ir.vbile.app.neshanchallenge.feature_navigator.domain.repository;

import org.neshan.common.model.LatLng;

import javax.annotation.Nullable;

import io.reactivex.Single;
import ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding.NeshanAddress;
import ir.vbile.app.neshanchallenge.core.domain.models.routing.DirectionWithCar;
import ir.vbile.app.neshanchallenge.feature_navigator.data.DirectionMode;

public interface NavigatorRepository {
    Single<DirectionWithCar> getDirections(
            @Nullable  @DirectionMode String type,
            String origin,
            String destination
    );
    Single<NeshanAddress> getReverseGeocoding(
            LatLng latLng
    );
}
