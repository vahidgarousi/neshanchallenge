package ir.vbile.app.neshanchallenge.core.presentation;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.neshan.common.model.LatLng;
import org.neshan.common.utils.PolylineEncoding;
import org.neshan.mapsdk.model.Marker;
import org.neshan.mapsdk.model.Polyline;
import org.neshan.servicessdk.direction.model.DirectionStep;
import org.neshan.servicessdk.direction.model.Route;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding.NeshanAddress;
import ir.vbile.app.neshanchallenge.core.domain.models.routing.DirectionWithCar;
import ir.vbile.app.neshanchallenge.core.domain.util.NeshanSingleObserver;
import ir.vbile.app.neshanchallenge.core.util.BaseViewModel;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case.GetDirectionsUseCase;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case.ReverseGeocodingUseCase;
import timber.log.Timber;

@HiltViewModel
public class MainViewModel extends BaseViewModel {
    @Inject
    GetDirectionsUseCase getDirectionsUseCase;
    @Inject
    ReverseGeocodingUseCase reverseGeocodingUseCase;

    @Inject
    public MainViewModel(GetDirectionsUseCase getDirectionsUseCase, ReverseGeocodingUseCase reverseGeocodingUseCase) {
        this.getDirectionsUseCase = getDirectionsUseCase;
        this.reverseGeocodingUseCase = reverseGeocodingUseCase;
    }

    private final MutableLiveData<Marker> _originMarker = new MutableLiveData<>();
    LiveData<Marker> originMarker = _originMarker;

    private final MutableLiveData<Route> _route = new MutableLiveData<>();
    public LiveData<Route> route = _route;

    final MutableLiveData<Marker> _destinationMarker = new MutableLiveData<>();
    LiveData<Marker> destinationMarker = _destinationMarker;

    private final MutableLiveData<NeshanAddress> _neshanAddress = new MutableLiveData<>();
    public LiveData<NeshanAddress> neshanAddress = _neshanAddress;

    private final MutableLiveData<Boolean> _showRouteSuggestion = new MutableLiveData<>();
    public LiveData<Boolean> showRouteSuggestion = _showRouteSuggestion;

    MutableLiveData<Polyline> oldPolyLine = new MutableLiveData<>();
    private final MutableLiveData<Polyline> _lastPolyline = new MutableLiveData<>();
    public LiveData<Polyline> lastPolyline = _lastPolyline;
    public final MutableLiveData<Boolean> overview = new MutableLiveData<>(false);

    public void getDestinationDetail(LatLng destinationLatLng) {
        _showRouteSuggestion.postValue(false);
        reverseGeocodingUseCase.getReverseGeocoding(destinationLatLng)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new NeshanSingleObserver<NeshanAddress>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull NeshanAddress neshanAddress) {
                        _neshanAddress.postValue(neshanAddress);
                        _showRouteSuggestion.postValue(true);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Timber.e(e);
                    }
                });
    }

    public void getRoute(LatLng origin, LatLng destination) {
        getDirectionsUseCase.getDirections(origin, destination)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new NeshanSingleObserver<DirectionWithCar>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull DirectionWithCar directionWithCar) {
                        _route.setValue(directionWithCar.routes.get(0));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Timber.e(e);
                    }
                });
    }


    public void setDestination(Marker marker) {
        getDestinationDetail(marker.getLatLng());
        _destinationMarker.postValue(marker);
        if (originMarker.getValue() != null) {
            getRoute(originMarker.getValue().getLatLng(), marker.getLatLng());
        }
    }

    public void setOrigin(Marker marker) {
        _originMarker.setValue(marker);
    }

    public void findRoute() {
        if (route.getValue() != null) {
            Route route = this.route.getValue();
            oldPolyLine.setValue(lastPolyline.getValue());
            ArrayList<LatLng> routeOverviewPolylinePoints = new ArrayList<>(PolylineEncoding.decode(route.getOverviewPolyline().getEncodedPolyline()));
            overview.setValue(true);
            _lastPolyline.setValue(new Polyline(routeOverviewPolylinePoints, getLineStyle()));
        }

    }
}