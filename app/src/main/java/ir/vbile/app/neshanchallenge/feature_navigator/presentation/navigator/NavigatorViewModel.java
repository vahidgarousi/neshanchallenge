package ir.vbile.app.neshanchallenge.feature_navigator.presentation.navigator;

import android.os.Handler;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.neshan.common.model.LatLng;
import org.neshan.common.utils.PolylineEncoding;
import org.neshan.mapsdk.model.Marker;
import org.neshan.mapsdk.model.Polyline;
import org.neshan.servicessdk.direction.model.DirectionResultLeg;
import org.neshan.servicessdk.direction.model.DirectionStep;
import org.neshan.servicessdk.direction.model.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import ir.vbile.app.neshanchallenge.core.util.BaseViewModel;
import ir.vbile.app.neshanchallenge.core.util.Constants;

@HiltViewModel
public class NavigatorViewModel extends BaseViewModel {

    public MutableLiveData<Marker> userMarker = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<LatLng>> _routeOverviewPolylinePoints = new MutableLiveData<>();
    public LiveData<ArrayList<LatLng>> routeOverviewPolylinePoints = _routeOverviewPolylinePoints;

    private final MutableLiveData<ArrayList<LatLng>> _decodedStepByStepPath = new MutableLiveData<>();
    public LiveData<ArrayList<LatLng>> decodedStepByStepPath = _decodedStepByStepPath;

    @Inject
    NavigatorViewModel() {
        timer = new Timer();
    }

    Timer timer;

    private final MutableLiveData<Route> _route = new MutableLiveData<>();
    public LiveData<Route> route = _route;

    private final MutableLiveData<Polyline> _overViewPolyLine = new MutableLiveData<>();
    public LiveData<Polyline> overviewPolyline = _overViewPolyLine;

    private final MutableLiveData<Polyline> _stepByStepPathPolyLine = new MutableLiveData<>();
    public LiveData<Polyline> stepByStepPathPolyLine = _stepByStepPathPolyLine;


    private final MutableLiveData<LatLng> _userCurrentLocation = new MutableLiveData<>();
    public LiveData<LatLng> userCurrentLocation = _userCurrentLocation;


    private final MutableLiveData<Float> _cameraBearing = new MutableLiveData<>();
    public LiveData<Float> cameraBearing = _cameraBearing;

    private final MutableLiveData<LatLng> _oldLatLng = new MutableLiveData<>();
    public LiveData<LatLng> oldLatLng = _oldLatLng;


    public void setUserLocation(Marker marker) {
        userMarker.setValue(marker);
    }

    ArrayList<LatLng> polyLines = new ArrayList<>();

    public void setRoute(Route route) {
        _route.setValue(route);
        ArrayList<LatLng> decodedStepByStepPath = new ArrayList<>();
        for (DirectionResultLeg leg : route.getLegs()) {
            for (DirectionStep directionStep : leg.getDirectionSteps()) {
                String encodedPolyline = directionStep.getEncodedPolyline();
                List<LatLng> decode = PolylineEncoding.decode(encodedPolyline);
                polyLines.addAll(decode);
                decodedStepByStepPath.addAll(new ArrayList<>(PolylineEncoding.decode(directionStep.getEncodedPolyline())));
            }
        }
        _decodedStepByStepPath.setValue(decodedStepByStepPath);
        _stepByStepPathPolyLine.setValue(new Polyline(decodedStepByStepPath, getLineStyle()));
        _overViewPolyLine.setValue(new Polyline(routeOverviewPolylinePoints.getValue(), getLineStyle()));
        startTimer();
    }

    int currentStep = 0;

    Handler handler = new Handler();

    private void startTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    Route route = _route.getValue();
                    if (route == null) return;
                    LatLng currentLatLang = polyLines.get(currentStep - 1);
                    _userCurrentLocation.setValue(polyLines.get(currentStep));
                    if (currentStep > 0) {
                        _oldLatLng.setValue(currentLatLang);
                    }
                });
                currentStep++;
            }
        }, 0, Constants.UPDATE_INTERVAL_IN_MILLISECONDS);
    }

    private double angleFromCoordinate(LatLng startLatLng, LatLng endLatLng) {
        double lat1 = startLatLng.getLatitude();
        double long1 = startLatLng.getLongitude();
        double lat2 = endLatLng.getLatitude();
        double long2 = endLatLng.getLongitude();
        double dLon = (long2 - long1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        double bearing = Math.atan2(y, x);
        bearing = Math.toDegrees(bearing);
        bearing = (bearing + 360) % 360;
        return bearing;
    }
}
