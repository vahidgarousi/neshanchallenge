package ir.vbile.app.neshanchallenge.feature_navigator.presentation.navigate_dialog;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;

import org.neshan.servicessdk.direction.model.Route;

import ir.vbile.app.neshanchallenge.R;
import ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding.NeshanAddress;
import ir.vbile.app.neshanchallenge.core.domain.models.routing.DirectionWithCar;
import timber.log.Timber;

public class RouteSuggestionDialog extends BottomSheetDialogFragment {
    private Route route;
    private NeshanAddress neshanAddress;

    private MaterialButton btnGetRouting;
    private MaterialButton btnStartRouting;
    private TextView tvDestinationName;
    private TextView tvAddress;
    private TextView tvDuration;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_route_suggestion, container, false);
        initViews(v);
        updateViews();
        return v;
    }

    private void updateViews() {
        try {
            String name = route.getLegs().get(0).getSummary();
            String duration = route.getLegs().get(0).getDuration().getText();
            tvDestinationName.setText(name);
            tvAddress.setText(neshanAddress.getState());
            tvDuration.setText(duration);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void initViews(View v) {
        btnGetRouting = v.findViewById(R.id.btn_routing);
        btnStartRouting = v.findViewById(R.id.btn_startRouting);
        tvDestinationName = v.findViewById(R.id.tv_destinationName);
        tvAddress = v.findViewById(R.id.tv_destinationAddress);
        tvDuration = v.findViewById(R.id.tv_Duration);
        btnStartRouting.setOnClickListener(view -> listener.startNavigating());
        btnGetRouting.setOnClickListener(view -> listener.findRoute());
    }

    private RouteSuggestionEventListener listener;

    public void setListener(RouteSuggestionEventListener listener) {
        this.listener = listener;
    }

    public void setRoute(Route route) {
        this.route = route;
        updateViews();
    }

    public void setNeshanAddress(NeshanAddress neshanAddress) {
        this.neshanAddress = neshanAddress;
        updateViews();
    }

    public interface RouteSuggestionEventListener {
        void startNavigating();

        void findRoute();
    }
}
