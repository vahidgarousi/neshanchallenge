package ir.vbile.app.neshanchallenge.di;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import com.chuckerteam.chucker.api.ChuckerCollector;
import com.chuckerteam.chucker.api.ChuckerInterceptor;
import com.chuckerteam.chucker.api.RetentionManager;
import com.google.gson.Gson;

import java.io.InputStream;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import ir.vbile.app.neshanchallenge.R;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
@InstallIn(SingletonComponent.class)
public class AppModule {

    @Provides
    @Singleton
    public static ChuckerInterceptor provideChuckerInterceptor(
            @ApplicationContext Context context
    ) {
        ChuckerCollector chuckerCollector = new ChuckerCollector(context, true, RetentionManager.Period.ONE_HOUR);
        return new ChuckerInterceptor.Builder(context)
                .collector(chuckerCollector)
                .maxContentLength(250000)
                .alwaysReadResponseBody(true)
                .build();
    }

    @Provides
    @Singleton
    public static HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }

    @Provides
    @Singleton
    public static OkHttpClient provideOkHttpClient(
            HttpLoggingInterceptor logging,
            AuthorizationHeaderInterceptor authorizationHeaderInterceptor,
            CommonHeadersInterceptor commonHeadersInterceptor,
            ChuckerInterceptor chuckerInterceptor
    ) {
        return new OkHttpClient.Builder()
                .addInterceptor(authorizationHeaderInterceptor)
                .addInterceptor(logging)
                .addInterceptor(chuckerInterceptor)
                .addInterceptor(commonHeadersInterceptor)
                .build();
    }

    @Provides
    @Singleton
    public static String provideNeshanMapApiKey(
            @ApplicationContext Context context
    ) {
        try {
            InputStream inputStream = context.getResources().openRawResource(R.raw.neshan);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);
            String apiKey = new String(b);
            inputStream.close();
            return apiKey;
        } catch (Exception e) {
            // e.printStackTrace();
            return "Error: can't show help.";
        }
    }

    @Provides
    @Singleton
    public static Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    public static SensorManager provideSensorManager(
            @ApplicationContext Context context
    ) {
        return (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    @Provides
    @Singleton
    @Named(Sensor.STRING_TYPE_MAGNETIC_FIELD)
    public static Sensor provideMagneticFieldSensor(
            SensorManager sensorManager
    ) {
        return sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    @Provides
    @Singleton
    @Named(Sensor.STRING_TYPE_ACCELEROMETER)
    public static Sensor provideAccelerometerSensor(
            SensorManager sensorManager
    ) {
        return sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }
}
