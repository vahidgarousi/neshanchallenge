package ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case;

import org.neshan.common.model.LatLng;

import io.reactivex.Single;
import ir.vbile.app.neshanchallenge.core.domain.models.routing.DirectionWithCar;
import ir.vbile.app.neshanchallenge.core.util.Constants;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.repository.NavigatorRepository;

public class GetDirectionsUseCase {

    private final NavigatorRepository repository;

    public GetDirectionsUseCase(
            NavigatorRepository repository
    ) {
        this.repository = repository;
    }

    public Single<DirectionWithCar> getDirections(
            LatLng originLatLng,
            LatLng destinationLatLng
    ) {
        String origin = originLatLng.getLatitude() + "," + originLatLng.getLongitude();
        String destination = destinationLatLng.getLatitude() + "," + destinationLatLng.getLongitude();
        return repository.getDirections(Constants.DIRECTION_MODE_CAR, origin, destination);
    }
}
