package ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AddressesItem{

	@SerializedName("components")
	private List<ComponentsItem> components;

	@SerializedName("formatted")
	private String formatted;

	public List<ComponentsItem> getComponents(){
		return components;
	}

	public String getFormatted(){
		return formatted;
	}
}