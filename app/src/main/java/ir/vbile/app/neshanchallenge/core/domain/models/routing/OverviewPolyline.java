package ir.vbile.app.neshanchallenge.core.domain.models.routing;

import com.google.gson.annotations.SerializedName;

public class OverviewPolyline{

	@SerializedName("points")
	private String points;

	public String getPoints(){
		return points;
	}
}