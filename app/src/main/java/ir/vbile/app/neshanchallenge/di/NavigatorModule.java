package ir.vbile.app.neshanchallenge.di;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import ir.vbile.app.neshanchallenge.feature_navigator.data.data_store.remote.NavigatorApi;
import ir.vbile.app.neshanchallenge.feature_navigator.data.repository.NavigatorRepositoryImpl;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.repository.NavigatorRepository;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case.GetDirectionsUseCase;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case.ReverseGeocodingUseCase;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public class NavigatorModule {
    @Provides
    @Singleton
    public static NavigatorApi provideNavigatorApi(
            OkHttpClient client
    ) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(NavigatorApi.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NavigatorApi.class);
    }

    @Provides
    @Singleton
    public static NavigatorRepository provideNavigatorRepository(
            NavigatorApi api
    ) {
        return new NavigatorRepositoryImpl(api);
    }

    @Provides
    @Singleton
    public static GetDirectionsUseCase provideGetDirectionsUseCase(
            NavigatorRepository repository
    ) {
        return new GetDirectionsUseCase(repository);
    }

    @Provides
    @Singleton
    public static ReverseGeocodingUseCase provideReverseGeocodingUseCase(
            NavigatorRepository repository
    ) {
        return new ReverseGeocodingUseCase(repository);
    }
}
