package ir.vbile.app.neshanchallenge.core.util;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ir.vbile.app.neshanchallenge.BuildConfig;

public abstract class BaseActivity<VM extends ViewModel> extends AppCompatActivity {

    protected VM vm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vm = new ViewModelProvider(this, getDefaultViewModelProviderFactory()).get(getViewModel());
        setContentView(getLayoutResId());
        onCreate(savedInstanceState, vm);
    }

    protected abstract Class<VM> getViewModel();

    protected abstract void onCreate(Bundle instance, VM vm);

    protected abstract @LayoutRes
    int getLayoutResId();

    protected void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
