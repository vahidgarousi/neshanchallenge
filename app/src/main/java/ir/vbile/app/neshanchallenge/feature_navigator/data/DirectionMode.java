package ir.vbile.app.neshanchallenge.feature_navigator.data;

import static ir.vbile.app.neshanchallenge.core.util.Constants.DIRECTION_MODE_CAR;
import static ir.vbile.app.neshanchallenge.core.util.Constants.DIRECTION_MODE_MOTORCYCLE;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({DIRECTION_MODE_CAR, DIRECTION_MODE_MOTORCYCLE})
@Retention(RetentionPolicy.SOURCE)
public @interface DirectionMode {}