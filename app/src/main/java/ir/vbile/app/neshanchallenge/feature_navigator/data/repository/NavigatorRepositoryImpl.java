package ir.vbile.app.neshanchallenge.feature_navigator.data.repository;

import org.neshan.common.model.LatLng;

import javax.annotation.Nullable;
import javax.inject.Inject;

import io.reactivex.Single;
import ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding.NeshanAddress;
import ir.vbile.app.neshanchallenge.core.domain.models.routing.DirectionWithCar;
import ir.vbile.app.neshanchallenge.feature_navigator.data.DirectionMode;
import ir.vbile.app.neshanchallenge.feature_navigator.data.data_store.remote.NavigatorApi;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.repository.NavigatorRepository;

public class NavigatorRepositoryImpl implements NavigatorRepository {
    private final NavigatorApi api;

    @Inject
    public NavigatorRepositoryImpl(NavigatorApi api) {
        this.api = api;
    }

    @Override
    public Single<DirectionWithCar> getDirections(
            @Nullable @DirectionMode String type,
            String origin,
            String destination
    ) {
        return api.getDirections(
                type,
                origin,
                destination
        );
    }

    @Override
    public Single<NeshanAddress> getReverseGeocoding(LatLng latLng) {
        return api.getReverseGeocoding(latLng.getLatitude(),latLng.getLongitude());
    }
}
