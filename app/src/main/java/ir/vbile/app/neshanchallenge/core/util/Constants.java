package ir.vbile.app.neshanchallenge.core.util;

public class Constants {
    // location updates interval - 1 sec
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    // fastest updates interval - 1 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    public static final String DIRECTION_MODE_CAR = "car";
    public static final String DIRECTION_MODE_MOTORCYCLE = "motorcycle";
    public static final String EXTRA_KEY_DATA = "extra_key_data";
}
