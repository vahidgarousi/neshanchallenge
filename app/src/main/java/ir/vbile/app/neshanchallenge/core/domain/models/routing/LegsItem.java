package ir.vbile.app.neshanchallenge.core.domain.models.routing;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LegsItem{

	@SerializedName("summary")
	private String summary;

	@SerializedName("duration")
	private Duration duration;

	@SerializedName("distance")
	private Distance distance;

	@SerializedName("steps")
	private List<StepsItem> steps;

	public String getSummary(){
		return summary;
	}

	public Duration getDuration(){
		return duration;
	}

	public Distance getDistance(){
		return distance;
	}

	public List<StepsItem> getSteps(){
		return steps;
	}
}