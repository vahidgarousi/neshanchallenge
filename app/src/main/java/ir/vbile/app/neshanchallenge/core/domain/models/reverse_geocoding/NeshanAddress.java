package ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class NeshanAddress{

	@SerializedName("formatted_address")
	private String formattedAddress;

	@SerializedName("addresses")
	private List<AddressesItem> addresses;

	@SerializedName("route_name")
	private String routeName;

	@SerializedName("city")
	private String city;

	@SerializedName("neighbourhood")
	private String neighbourhood;

	@SerializedName("route_type")
	private String routeType;

	@SerializedName("in_odd_even_zone")
	private boolean inOddEvenZone;

	@SerializedName("state")
	private String state;

	@SerializedName("place")
	private Object place;

	@SerializedName("in_traffic_zone")
	private boolean inTrafficZone;

	@SerializedName("municipality_zone")
	private String municipalityZone;

	@SerializedName("status")
	private String status;

	public String getFormattedAddress(){
		return formattedAddress;
	}

	public List<AddressesItem> getAddresses(){
		return addresses;
	}

	public String getRouteName(){
		return routeName;
	}

	public String getCity(){
		return city;
	}

	public String getNeighbourhood(){
		return neighbourhood;
	}

	public String getRouteType(){
		return routeType;
	}

	public boolean isInOddEvenZone(){
		return inOddEvenZone;
	}

	public String getState(){
		return state;
	}

	public Object getPlace(){
		return place;
	}

	public boolean isInTrafficZone(){
		return inTrafficZone;
	}

	public String getMunicipalityZone(){
		return municipalityZone;
	}

	public String getStatus(){
		return status;
	}
}