package ir.vbile.app.neshanchallenge.feature_navigator.presentation.navigator;

import static ir.vbile.app.neshanchallenge.core.util.Constants.EXTRA_KEY_DATA;
import static ir.vbile.app.neshanchallenge.core.util.Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;
import static ir.vbile.app.neshanchallenge.core.util.Constants.UPDATE_INTERVAL_IN_MILLISECONDS;

import android.Manifest;
import android.content.IntentSender;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.carto.styles.MarkerStyle;
import com.carto.styles.MarkerStyleBuilder;
import com.carto.utils.BitmapUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.neshan.common.model.LatLng;
import org.neshan.mapsdk.MapView;
import org.neshan.mapsdk.model.Marker;
import org.neshan.servicessdk.direction.model.Route;

import java.text.DateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.hilt.android.AndroidEntryPoint;
import ir.vbile.app.neshanchallenge.R;
import ir.vbile.app.neshanchallenge.core.util.BaseActivity;
import timber.log.Timber;

@AndroidEntryPoint
public class NavigatorActivity extends BaseActivity<NavigatorViewModel> {
    // map UI element
    MapView map;
    TextView tvReachTimeValue;
    TextView tvDestinationValue;

    // used to track request permissions
    final int REQUEST_CODE = 123;

    // User's current location
    private Location userLocation;
    private FusedLocationProviderClient fusedLocationClient;
    private SettingsClient settingsClient;
    private LocationRequest locationRequest;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationCallback locationCallback;
    private String lastUpdateTime;
    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;


    // variable that hold camera bearing
    float cameraBearing;

    boolean isCameraRotationEnable = true;

    @Inject
    SensorManager sensorManager;

    @Inject
    @Named(Sensor.STRING_TYPE_ACCELEROMETER)
    Sensor sensorAccelerometer;

    @Inject
    @Named(Sensor.STRING_TYPE_MAGNETIC_FIELD)
    Sensor sensorMagneticField;

    @Inject
    Gson gson;

    @Override
    protected Class<NavigatorViewModel> getViewModel() {
        return NavigatorViewModel.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_navigator;
    }

    @Override
    protected void onCreate(Bundle instance, NavigatorViewModel navigatorViewModel) {
        Route route = gson.fromJson(getIntent().getStringExtra(EXTRA_KEY_DATA), Route.class);
        vm.setRoute(route);
        observeData();
    }

    private void observeData() {
        vm.stepByStepPathPolyLine.observe(this, polyline -> map.addPolyline(polyline));
        vm.route.observe(this, route -> {
            tvReachTimeValue.setText(route.getLegs().get(0).getDuration().getText());
            tvDestinationValue.setText(route.getLegs().get(0).getDistance().getText());
        });
        vm.userCurrentLocation.observe(this, this::focusOnUserLocation);
        vm.cameraBearing.observe(this, value ->
                map.setBearing(value, 0.5f)
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        // everything related to ui is initialized here
        initLayoutReferences();
    }

    private void initLayoutReferences() {
        // Initializing views
        initViews();
        // Initializing mapView element
        initMap();
    }

    private void initViews() {
        map = findViewById(R.id.map);
        tvReachTimeValue = findViewById(R.id.tvReachTimeValue);
        tvDestinationValue = findViewById(R.id.tvDestinationValue);
    }

    private void initMap() {
        map.setZoom(14, 0);
        map.setPoiEnabled(true);
        map.getSettings().setMapRotationEnabled(true);
        map.setOnCameraMoveListener(() -> {
            // updating seek bar with new camera bearing value
            if (map.getBearing() < 0) {
                cameraBearing = (180 + map.getBearing()) + 180;
            } else {
                cameraBearing = map.getBearing();
            }
        });

    }

    private Marker addMarker(LatLng latLng) {
        //remove existing marker from map
        if (vm.userMarker.getValue() != null) {
            map.removeMarker(vm.userMarker.getValue());
        }
        // Creating marker style. We should use an object of type MarkerStyleCreator, set all features on it
        // and then call buildStyle method on it. This method returns an object of type MarkerStyle
        MarkerStyleBuilder markStCr = new MarkerStyleBuilder();
        markStCr.setSize(30f);
        markStCr.setBitmap(BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_cluster_marker_blue)));
        MarkerStyle markSt = markStCr.buildStyle();
        // Creating user marker
        Marker marker = new Marker(latLng, markSt);
        // Adding user marker to map!
        map.addMarker(marker);
        return marker;
    }

    public void focusOnUserLocation(LatLng userLatLng) {
        vm.setUserLocation(addMarker(userLatLng));
        map.moveCamera(new LatLng(userLatLng.getLatitude(), userLatLng.getLongitude()), 0.5f);
        if (vm.oldLatLng.getValue() != null) {
            map.setZoom(16, 0.5f);
            map.setTilt(35, 1f);
        }
    }
}