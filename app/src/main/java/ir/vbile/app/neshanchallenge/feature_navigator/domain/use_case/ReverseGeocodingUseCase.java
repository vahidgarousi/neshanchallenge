package ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case;

import org.neshan.common.model.LatLng;

import io.reactivex.Single;
import ir.vbile.app.neshanchallenge.core.domain.models.reverse_geocoding.NeshanAddress;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.repository.NavigatorRepository;

public class ReverseGeocodingUseCase {
    private final NavigatorRepository repository;

    public ReverseGeocodingUseCase(NavigatorRepository repository) {
        this.repository = repository;
    }

    public Single<NeshanAddress> getReverseGeocoding(LatLng latLng) {
        return repository.getReverseGeocoding(latLng);
    }
}
