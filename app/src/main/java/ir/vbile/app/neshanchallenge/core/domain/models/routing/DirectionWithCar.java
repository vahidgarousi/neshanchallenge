package ir.vbile.app.neshanchallenge.core.domain.models.routing;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import org.neshan.servicessdk.direction.model.Route;

public class DirectionWithCar{
	@SerializedName("routes")
	public List<Route> routes;
}