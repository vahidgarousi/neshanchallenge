package ir.vbile.app.neshanchallenge.core.presentation;

import static ir.vbile.app.neshanchallenge.core.util.Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS;
import static ir.vbile.app.neshanchallenge.core.util.Constants.UPDATE_INTERVAL_IN_MILLISECONDS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.carto.styles.MarkerStyle;
import com.carto.styles.MarkerStyleBuilder;
import com.carto.utils.BitmapUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.neshan.common.model.LatLng;
import org.neshan.mapsdk.MapView;
import org.neshan.mapsdk.model.Marker;
import org.neshan.servicessdk.direction.model.Route;

import java.text.DateFormat;
import java.util.Date;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import ir.vbile.app.neshanchallenge.R;
import ir.vbile.app.neshanchallenge.core.util.BaseActivity;
import ir.vbile.app.neshanchallenge.core.util.Constants;
import ir.vbile.app.neshanchallenge.feature_navigator.domain.use_case.GetDirectionsUseCase;
import ir.vbile.app.neshanchallenge.feature_navigator.presentation.navigate_dialog.RouteSuggestionDialog;
import ir.vbile.app.neshanchallenge.feature_navigator.presentation.navigator.NavigatorActivity;
import timber.log.Timber;

@AndroidEntryPoint
public class MainActivity extends BaseActivity<MainViewModel> {

    @Inject
    public GetDirectionsUseCase getDirectionsUseCase;


    @Override
    protected Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    // map UI element
    MapView map;
    FloatingActionButton btnFocusOnUserLocation;

    // used to track request permissions
    final int REQUEST_CODE = 123;

    // User's current location
    private Location userLocation;
    private FusedLocationProviderClient fusedLocationClient;
    private SettingsClient settingsClient;
    private LocationRequest locationRequest;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationCallback locationCallback;
    // boolean flag to toggle the ui
    private Boolean shouldFocusOnUserLocation = true;

    @Inject
    RouteSuggestionDialog routeSuggestionDialog;

    @Inject
    Gson gson;

    @Override
    protected void onCreate(Bundle instance, MainViewModel vm) {
        configRouteSuggestionDialog();
        observeData();
    }

    private void observeData() {
        vm.route.observe(this, routeSuggestionDialog::setRoute);
        vm.oldPolyLine.observe(this, polyline -> {
            if (polyline != null) {
                map.removePolyline(polyline);
            }
        });
        vm.lastPolyline.observe(this, polyline -> {
            map.addPolyline(polyline);
            mapSetPosition(true);
        });
        vm.neshanAddress.observe(this, routeSuggestionDialog::setNeshanAddress);
        vm.showRouteSuggestion.observe(this, shouldShowDialog -> {
            if (shouldShowDialog) {
                if (routeSuggestionDialog.isVisible()) return;
                routeSuggestionDialog.show(getSupportFragmentManager(), null);
            }
        });
    }

    private void mapSetPosition(boolean overview) {
        if (vm.originMarker.getValue() != null && vm.destinationMarker.getValue() != null) {
            Marker originMarker = vm.originMarker.getValue();
            Marker destinationMarker = vm.destinationMarker.getValue();
            double centerFirstMarkerX = originMarker.getLatLng().getLatitude();
            double centerFirstMarkerY = originMarker.getLatLng().getLongitude();
            if (overview) {
                double centerFocalPositionX = (centerFirstMarkerX + destinationMarker.getLatLng().getLatitude()) / 2;
                double centerFocalPositionY = (centerFirstMarkerY + destinationMarker.getLatLng().getLongitude()) / 2;
                map.moveCamera(new LatLng(centerFocalPositionX, centerFocalPositionY), 0.5f);
                map.setZoom(13, 0.5f);
            } else {
                map.moveCamera(new LatLng(centerFirstMarkerX, centerFirstMarkerY), 0.5f);
                map.setZoom(18, 0.5f);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // everything related to ui is initialized here
        initLayoutReferences();
        initLocation();
        startReceivingLocationUpdates();
        map.setOnMapLongClickListener(latLng -> {
            Marker marker = addDestinationMarker(latLng);
            vm.setDestination(marker);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(this, locationSettingsResponse -> {
                    Timber.i("All location settings are satisfied.");
                    //noinspection MissingPermission
                    fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                    onLocationChange();
                })
                .addOnFailureListener(this, e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Timber.i("Location settings are not satisfied. Attempting to upgrade " +
                                    "location settings ");
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult(MainActivity.this, REQUEST_CODE);
                            } catch (IntentSender.SendIntentException sie) {
                                Timber.i("PendingIntent unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Location settings are inadequate, and cannot be fixed here. Fix in Settings.";
                            Timber.i(errorMessage);
                            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }

                    onLocationChange();
                });
    }

    private void stopLocationUpdates() {
        // Removing location updates
        fusedLocationClient
                .removeLocationUpdates(locationCallback)
                .addOnCompleteListener(this, task -> Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show());
    }

    private void startReceivingLocationUpdates() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void initLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        settingsClient = LocationServices.getSettingsClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                userLocation = locationResult.getLastLocation();
                onLocationChange();
            }
        };
        locationRequest = LocationRequest.create()
                .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
                .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();
    }

    private void onLocationChange() {
        if (userLocation != null) {
            vm.setOrigin(addOriginMarker(new LatLng(userLocation.getLatitude(), userLocation.getLongitude())));
            focusOnUserLocation();
        }
    }

    private void configRouteSuggestionDialog() {
        routeSuggestionDialog.setListener(new RouteSuggestionDialog.RouteSuggestionEventListener() {
            @Override
            public void startNavigating() {
                // Start Navigating
                startNavigatorActivity();
            }

            @Override
            public void findRoute() {
                // Start Routing
                vm.findRoute();
            }
        });
    }

    private void startNavigatorActivity() {
        Route route = vm.route.getValue();
        Intent intent = new Intent(this, NavigatorActivity.class);
        String routeString = gson.toJson(route);
        intent.putExtra(Constants.EXTRA_KEY_DATA, routeString);
        startActivity(intent);
    }

    // Initializing layout references (views, map and map events)
    private void initLayoutReferences() {
        // Initializing views
        initViews();
        // Initializing mapView element
        initMap();
        btnFocusOnUserLocation.setOnClickListener(view ->
                focusOnUserLocation()
        );
    }

    private void initMap() {
        // Setting map focal position to a fixed position and setting camera zoom
        map.setZoom(14, 0);
    }

    private void initViews() {
        map = findViewById(R.id.map);
        btnFocusOnUserLocation = findViewById(R.id.btn_focusOnUserLocation);
    }

    public void focusOnUserLocation() {
        if (vm.originMarker.getValue() != null && shouldFocusOnUserLocation) {
            LatLng userLatLng = vm.originMarker.getValue().getLatLng();
            map.moveCamera(new LatLng(userLatLng.getLatitude(), userLatLng.getLongitude()), 0.25f);
            map.setZoom(15, 0.25f);
            shouldFocusOnUserLocation = false;
        }
    }


    // This method gets a LatLng as input and adds a marker on that position
    private Marker addOriginMarker(LatLng loc) {
        //remove existing marker from map
        if (vm.originMarker.getValue() != null) {
            map.removeMarker(vm.originMarker.getValue());
        }
        // Creating marker style. We should use an object of type MarkerStyleCreator, set all features on it
        // and then call buildStyle method on it. This method returns an object of type MarkerStyle
        MarkerStyleBuilder markStCr = new MarkerStyleBuilder();
        markStCr.setSize(30f);
        markStCr.setBitmap(BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker)));
        MarkerStyle markSt = markStCr.buildStyle();
        // Creating user marker
        Marker marker = new Marker(loc, markSt);
        // Adding user marker to map!
        map.addMarker(marker);
        return marker;
    }

    // This method gets a LatLng as input and adds a marker on that position
    private Marker addDestinationMarker(LatLng loc) {
        //remove existing marker from map
        if (vm.destinationMarker.getValue() != null) {
            map.removeMarker(vm.destinationMarker.getValue());
        }
        // Creating marker style. We should use an object of type MarkerStyleCreator, set all features on it
        // and then call buildStyle method on it. This method returns an object of type MarkerStyle
        MarkerStyleBuilder markStCr = new MarkerStyleBuilder();
        markStCr.setSize(30f);
        markStCr.setBitmap(BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_cluster_marker_blue)));
        MarkerStyle markSt = markStCr.buildStyle();
        // Creating user marker
        Marker marker = new Marker(loc, markSt);
        // Adding user marker to map!
        map.addMarker(marker);
        return marker;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check for the integer request code originally supplied to startResolutionForResult().
        if (requestCode == REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Timber.e("User agreed to make required location settings changes.");
                    // Nothing to do. startLocationUpdates() gets called in onResume again.
                    break;
                case Activity.RESULT_CANCELED:
                    Timber.e("User chose not to make required location settings changes.");
                    break;
            }
        }
    }
}